<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\Promocion;
use common\models\search\PromocionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\utils\UtilImage;

/**
 * PromocionController implements the CRUD actions for Promocion model.
 */
class PromocionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'index', 'update', 'view', 'delete', 'publicar', 'verpublicar'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Promocion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PromocionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Promocion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Promocion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Promocion();
        $model->setScenario('create');
        
        if ($model->load(Yii::$app->request->post())) {
            $folder = UtilImage::getFolderPromocionImagen();
            UtilImage::uploadImage($model, $folder, 'url_imagen');
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id_promocion]);
            } else {
                var_dump($model->getErrors());
                die();
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Promocion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $url_image = $model->url_imagen;
        if ($model->load(Yii::$app->request->post())) {
            $model->url_imagen = $model->url_imagen? $model->url_imagen : $url_image;//prevent error delete.
            $folder = UtilImage::getFolderPromocionImagen();
            $upload = UtilImage::uploadImage($model, $folder, 'url_imagen');
            if($upload){
                UtilImage::deleteImage($folder, $url_image);
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id_promocion]);
            } else {
                var_dump($model->getErrors());
                die();
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Promocion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        UtilImage::deleteImage(UtilImage::getFolderPromocionImagen(), $model->url_imagen);
        return $this->redirect(['index']);
    }
    
    public function actionPublicar($id){
        $model = $this->findModel($id);
        $model->publicar();
        return $this->redirect(['verpublicar', 'id' => $model->id_promocion]);
    }
    
    public function actionVerpublicar($id){
        $model = $this->findModel($id);
        return $this->render('verpublicar', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Promocion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Promocion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Promocion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
