<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\Negocio;
use common\models\search\NegocioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\utils\UtilImage;

/**
 * NegocioController implements the CRUD actions for Negocio model.
 */
class NegocioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'index', 'update', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Negocio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NegocioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Negocio model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Negocio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Negocio();
        $model->setScenario('create');
        
        if ($model->load(Yii::$app->request->post())) {
            $folder = \common\utils\UtilImage::getFolderNegocioImagen();
            \common\utils\UtilImage::uploadImage($model, $folder, 'url_imagen');
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id_negocio]);
            } else {
                var_dump($model->getErrors());
                die();
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Negocio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $url_image = $model->url_imagen;
        if ($model->load(Yii::$app->request->post())) {
            $model->url_imagen = $model->url_imagen? $model->url_imagen : $url_image;//prevent error delete.
            $folder = UtilImage::getFolderNegocioImagen();
            $upload = UtilImage::uploadImage($model, $folder, 'url_imagen');
            if($upload){
                UtilImage::deleteImage($folder, $url_image);
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id_negocio]);
            } else {
                var_dump($model->getErrors());
                die();
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Negocio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        UtilImage::deleteImage(UtilImage::getFolderNegocioImagen(), $model->url_imagen);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Negocio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Negocio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Negocio::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
