<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;

class WsController extends Controller{
    
    
    public function actionTest(){
        echo 'vivilanoche-ws-test-ok';
    }
    
    public function actionCategorias(){
        $data = array();
        $all = \common\models\Categoria::find()->orderBy("orden")->all();
        foreach ($all as $model) {
            $data[] = $model->attributes;
        }
        echo json_encode($data);
    }
    
    public function actionNegocios($id_ciudad, $id_categoria){
        $data = array();
        $all = \common\models\Negocio::findAll(['id_ciudad' => $id_ciudad, 'id_categoria' => $id_categoria]);
        foreach ($all as $model) {
            $data[] = $model->attributes;
        }
        echo json_encode($data);
    }
    
    public function actionPromocion($id_promocion){
        $model = \common\models\Promocion::findOne($id_promocion);
        $data = $model->attributes;
        echo json_encode($data);
    }
    
    public function actionPromociones(){
        $data = array();
        $all = \common\models\Promocion::find()->orderBy("id_promocion desc")->all();
        foreach ($all as $model) {
            $data[] = $model->attributes;
        }
        echo json_encode($data);
    }
}
