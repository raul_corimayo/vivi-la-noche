<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Noticia */

$this->title = $model;
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="noticia-view">
    
    <h1>Ver Noticia</h1>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_noticia',
            'titulo',
            //'url_imagen:url',// mostrar imagen
            [
                'label' => 'Imagen',
                'format' => 'image',
                'value'=>function($data) { return $data->getUrlImagen(); },
            ], 
            'contenido:ntext',// mostrar un poco
            [
                'label' => 'Creación',
                'value'=>function($data) { return \common\utils\UtilFecha::toDMYHMS($data->creacion); },
            ],
            [
                'label' => 'Publicación',
                'value'=>function($data) { return \common\utils\UtilFecha::toDMY($data->publicacion); },
            ],
        ],
    ]) ?>
    
    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_noticia], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_noticia], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => common\utils\UtilImage::getDeletedModelMessage(),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    

</div>
