<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Noticia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="noticia-form">

    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    <!--IMAGE begin-->
    <?php if($model->url_imagen){?>
    <img src="<?php echo $model->getUrlImagen();?>">
    <?php }?>
    <?= $form->field($model, 'url_imagen')->widget(\kartik\widgets\FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png'],'showUpload' => false,],
    ]);   ?>
    <!--IMAGE end-->

    <?= $form->field($model, 'contenido')->textarea(['rows' => 6]) ?>

    <?php //echo $form->field($model, 'creacion')->textInput() ?>

    <?= $form->field($model, 'publicacion')->textInput() ?>
    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
