<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\NoticiaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticia-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar Noticia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_noticia',
            [
                'label' => 'Creación',
                'value'=>function($data) { return \common\utils\UtilFecha::toDMYHMS($data->creacion); },
            ],
            [
                'label' => 'Publicación',
                'value'=>function($data) { return \common\utils\UtilFecha::toDMY($data->publicacion); },
            ],
            'titulo',
            //'url_imagen:url',
            [
                'label' => 'Imagen',
                'format' => 'image',
                'value'=>function($data) { return $data->getUrlImagen(); },
            ], 
            //'contenido:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
