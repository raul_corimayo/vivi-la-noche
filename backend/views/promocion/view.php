<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Promocion */

$this->title = $model;
$this->params['breadcrumbs'][] = ['label' => 'Promociones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="promocion-view">

    <?php //if($model->url_imagen){?>
    <!--<img src="<?php //echo $model->getUrlImagen();?>">-->
    <?php //}?>
    
    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_promocion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_promocion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => common\utils\UtilImage::getDeletedModelMessage(),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Publicar', ['publicar', 'id' => $model->id_promocion], ['class' => 'btn btn-info']) ?>
    </p>
    
    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_promocion',
            'nombre',
            'negocio',// mostrar negocio
            //'url_imagen:url',// mostrar imagen minificada
            [
                'label' => 'Imagen',
                'format' => 'image',
                'value'=>function($data) { return $data->getUrlImagen(); },
            ], 
            'descripcion', 
        ],
    ]) ?>
    
    
    
    

</div>