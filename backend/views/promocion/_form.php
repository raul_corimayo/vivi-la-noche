<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Promocion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promocion-form">

    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_negocio')->widget(kartik\select2\Select2::classname(), [
        'data' => ArrayHelper::map(common\models\Negocio::find()->all(), 'id_negocio', 'nombre'),
        'language' => 'es',
        'options' => ['placeholder' => 'Seleccionar ...'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])
    ?>
    
    <!--IMAGE begin-->
    <?php if($model->url_imagen){?>
    <img src="<?php echo $model->getUrlImagen();?>">
    <?php }?>
    <?= $form->field($model, 'url_imagen')->widget(\kartik\widgets\FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png'],'showUpload' => false,],
    ]);   ?>
    <!--IMAGE end-->

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
