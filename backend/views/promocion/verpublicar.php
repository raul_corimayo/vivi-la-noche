<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Promocion */

$this->title = 'Promoción publicada : '.$model;
$this->params['breadcrumbs'][] = ['label' => 'Promociones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocion-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= Html::a('Volver', ['view', 'id' => $model->id_promocion], ['class' => 'btn btn-primary']) ?>
    
    <div class="jumbotron">
        <h1>Publicada</h1>
        <?php if($model->url_imagen){?>
        <img src="<?php echo $model->getUrlImagen();?>">
        <?php }?>
        <p><?php echo $model->descripcion;?></p>
    </div>
    
    
    

</div>
