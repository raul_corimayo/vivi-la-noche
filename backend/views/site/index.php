<?php


use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Viva la noche';
?>
<script src="https://kit.fontawesome.com/8010d066cd.js" crossorigin="anonymous"></script>
<div class="site-index">

    <!--<div class="jumbotron">-->
        <!--<h1>Vivila noche . </h1>-->
        <!---->
        <!--<p class="lead">You have successfully created your Yii-powered application.</p>-->

        <!--<p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>-->
    <!--</div>-->

    <div class="body-content">
        <br/>
        <div class="row">
            <div class="col-lg-4" >
                <center>
                <h2>Negocios</h2>
                <?= Html::a('<i class="fas fa-store fa-10x"></i>', ['negocio/index']) ?>
                <br/><br/><br/>
                </center>
            </div>
            <div class="col-lg-4">
                <center>
                <h2>Ciudades</h2>
                <?= Html::a('<i class="fas fa-globe-americas fa-10x"></i>', ['ciudad/index']) ?>
                <br/><br/><br/>
                </center>
            </div>
            <div class="col-lg-4">
                <center>
                <h2>Promociones</h2>
                <?= Html::a('<i class="fas fa-bullhorn fa-10x"></i>', ['promocion/index']) ?>
                <br/><br/><br/>
                </center>
            </div>
            <div class="col-lg-4">
                <center>
                <h2>Noticias</h2>
                <?= Html::a('<i class="far fa-newspaper fa-10x"></i>', ['noticia/index']) ?>
                <br/><br/><br/>
                </center>
            </div>
            <div class="col-lg-4">
                <center>
                <h2>Categorias</h2>
                <?= Html::a('<i class="fas fa-sitemap fa-10x"></i>', ['categoria/index']) ?>
                <br/><br/><br/>
                </center>
            </div>
        </div>

    </div>
</div>
