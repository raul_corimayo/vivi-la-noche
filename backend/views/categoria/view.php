<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Categoria */

$this->title = $model;
$this->params['breadcrumbs'][] = ['label' => 'Categorías', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="categoria-view">

    <?php if($model->url_icono){?>
    <img src="<?php echo $model->getUrlIcono();?>">
    <?php }?>
        
    <h1>    
        <?= Html::encode($this->title) ?>
    </h1>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_categoria',
            'orden',
            //'nombre',
            //'url_icono:url',
            [
                'label' => 'Imagen',
                'format' => 'image',
                'value'=>function($data) { return $data->getUrlIcono(); },
            ], 
        ],
    ]) ?>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_categoria], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_categoria], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => common\utils\UtilImage::getDeletedModelMessage(),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    

</div>
