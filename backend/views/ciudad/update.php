<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Ciudad */

$this->title = 'Modificar Ciudad: ' . $model;
$this->params['breadcrumbs'][] = ['label' => 'Ciudades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model, 'url' => ['view', 'id' => $model->id_ciudad]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="ciudad-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
