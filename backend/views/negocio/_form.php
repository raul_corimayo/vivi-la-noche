<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Negocio */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="negocio-form">

    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'id_ciudad')->widget(kartik\select2\Select2::classname(), [
        'data' => ArrayHelper::map(common\models\Ciudad::find()->all(), 'id_ciudad', 'nombre'),
        'language' => 'es',
        'options' => ['placeholder' => 'Seleccionar ...'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])
    ?>

    <?= $form->field($model, 'id_categoria')->widget(kartik\select2\Select2::classname(), [
        'data' => ArrayHelper::map(common\models\Categoria::find()->all(), 'id_categoria', 'nombre'),
        'language' => 'es',
        'options' => ['placeholder' => 'Seleccionar ...'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])
    ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <!--IMAGE begin-->
    <?php if($model->url_imagen){?>
    <img src="<?php echo $model->getUrlImagen();?>">
    <?php }?>
    <?= $form->field($model, 'url_imagen')->widget(\kartik\widgets\FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png'],'showUpload' => false,],
    ]);   ?>
    <!--IMAGE end-->

    <?= $form->field($model, 'url_video')->textInput(['maxlength' => true,]) ?>

    <?= $form->field($model, 'geo_latitud')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'geo_longitud')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
