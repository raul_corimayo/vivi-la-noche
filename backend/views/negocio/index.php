<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\NegocioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Negocios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="negocio-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar Negocio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_negocio',
            'ciudad',
            'categoria',
            'nombre',
            //'url_imagen:url',
            [
                'label' => 'Imagen',
                'format' => 'image',
                'value'=>function($data) { return $data->getUrlImagen(); },
            ],            
            'url_video:url',
            'geo_latitud',
            'geo_longitud',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
