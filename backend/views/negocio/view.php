<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Negocio */

$this->title = $model;
$this->params['breadcrumbs'][] = ['label' => 'Negocios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<style>
    /* Set the size of the div element that contains the map */
    #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
        background-color: grey;
    }
</style>
<script>
    // Initialize and add the map
    function initMap() {
        // The location of Uluru
        var uluru = {lat: <?php echo $model->geo_latitud; ?>, lng: <?php echo $model->geo_longitud; ?>};
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 17, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDn9FBQs892LQBSH67xQkRTSYDCt8BTQnI&callback=initMap">
</script>
<div class="negocio-view">

    <h1><?= Html::encode($this->title) ?></h1>



    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_negocio',
            'ciudad', //mostrar ciudad
            'categoria', //mostrar cat
            'nombre',
            //'url_imagen:url',//mostrar imagen del negocio minificada
            [
                'label' => 'Imagen',
                'format' => 'image',
                'value' => function($model) {
                    return $model->getUrlImagen();
                },
            ],
            'url_video', // mostrar vista previa del video
            'geo_latitud', // mostrar juntos ?
            'geo_longitud', // mostrar juntos ?
        ],
    ])
    ?>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_negocio], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Eliminar', ['delete', 'id' => $model->id_negocio], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => common\utils\UtilImage::getDeletedModelMessage(),
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <iframe id="ytplayer" type="text/html" width="640" height="360" 
            src="http://www.youtube.com/embed/<?php echo $model->getVideoID(); ?>" 
            frameborder="0">
    </iframe>

    <h3>Mapa</h3>
    <div id="map">

    </div>

</div>
