<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\NegocioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="negocio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_negocio') ?>

    <?= $form->field($model, 'id_ciudad') ?>

    <?= $form->field($model, 'id_categoria') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'url_imagen') ?>

    <?php // echo $form->field($model, 'url_video') ?>

    <?php // echo $form->field($model, 'geo_latitud') ?>

    <?php // echo $form->field($model, 'geo_longitud') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
