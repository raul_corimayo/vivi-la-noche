<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Negocio */

$this->title = 'Modificar Negocio: ' . $model;
$this->params['breadcrumbs'][] = ['label' => 'Negocios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model, 'url' => ['view', 'id' => $model->id_negocio]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="negocio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
