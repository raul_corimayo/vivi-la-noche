<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    
    
    
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <?= $content ?>
    <footer class="container-narrow" style="margin-top:0;background-color:#333;color:#555;text-align:center;padding:50px;">
        <h1>
            Viví la noche <?php echo date('Y')?>
        </h1>
        <hr style="border-color:#444;"/>
        <p>
            < Información de contacto >
        </p>
        <hr style="border-color:#444;"/>
        <div class="row">
            <div class="col-lg-2">
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
            </div>
            <div class="col-lg-2">
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
            </div>
            <div class="col-lg-2">
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
            </div>
            <div class="col-lg-2">
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
            </div>
            <div class="col-lg-2">
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
            </div>
            <div class="col-lg-2">
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
                <p>********* *** * * * ************</p>
            </div>
        </div>
        <hr style="border-color:#444;"/>
    </footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
