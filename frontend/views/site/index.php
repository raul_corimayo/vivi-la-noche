<?php

/* @var $this yii\web\View */

$this->title = 'Viví la noche';
use yii\bootstrap\Modal;

?>
<style>
    .noticia{
        transition-duration: 0.3s;
    }
    .noticia:hover{
        box-shadow: 0px 0px 15px grey;
    }
</style>

<div class="site-index jumbotron" style='
     background-image: url("images/girl.jpg");
     background-size: cover; 
     background-position: center center;
     margin-top:0px;
     min-height:100vh;'>
    
    <div style="color:white;margin-top:10vh;margin-bottom:0;">
        <p><span style="font-size:15vw;"><b>Viví la noche</b></span></p> 
    </div>
</div>

<div class="container" style="min-height:100vh;">
    <div class="pull-right">
        <a href="admin" class="btn btn-default">
            <span class="glyphicon glyphicon-user"></span> Administrador
        </a>
    </div>
       
    
    
    <div class="jumbotron">
        <h1> <span class="glyphicon glyphicon-wrench"></span> </h1>
        <p>Página en construcción</p>
    </div>    
    
    <div class="jumbotron">
        <a href="downloads/vivilanoche.apk">
            <img src="images/android.png">
        </a>
		<p>App actualizada al 20/12/2020</p>
        <!--
        <a href="downloads/vivilanoche2.apk">
            <img src="images/android.png">
        </a>
        <p>Descargar App ...</p>
        -->
    </div>
    
    <?php if ($noticias) { ?>
    <div class="jumbotron"><h1>Noticias</h1></div>
    <?php } ?>
    
    <div class="row" >
        <?php $i=0; ?>
        <?php foreach ($noticias as $noticia) {?>
            <div class="col-lg-6 col-sm-6">
                <a onclick="$('#w<?php echo $i; ?>').modal('show')">
                    <img src="<?php echo $noticia->getUrlImagen(); ?>" class="noticia" style="width:100%;">
                </a>
                <p><?php echo $noticia->titulo; ?></p>
                <p><?php //echo $noticia->contenido; ?></p>
                <br/>
            </div>
            <?php $publicacion = \common\utils\UtilFecha::toDMY($noticia->publicacion); ?>
            <?php Modal::begin(['header' => ''
                . '<span style="font-size: xx-large">'.$noticia->titulo.' </span> '
                . ' <span style="color:lightgray;"> '.$publicacion.' </span>',]);
                //echo '<p>'.$noticia->publicacion.'</p><hr/>';
                echo '<img src="'.$noticia->getUrlImagen().'" style="width:100%;">';
                echo $noticia->getContenidoHtml();
            Modal::end();?>
            <?php $i++; ?>
        <?php } ?>
    </div>
    
</div>
