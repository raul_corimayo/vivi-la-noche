<?php

namespace common\utils;

use Yii;

/**
 * Description of Util
 *
 * @author RAUL
 */
class UtilFirebase {

    public static function publicarPromocion(\common\models\Promocion $promocion) {
        $message = "$promocion->nombre de $promocion->negocio";
        $title = 'Promocion ' . $promocion->id_promocion;


        define('API_ACCESS_KEY', 'AAAAzJ6qf_s:APA91bEX2AYvufNgKlggk_2_8PtxBKpviK6F-VKL0L7K41qyma4thjusYJfbrHFsChT_wRP1zijMTsxP3o4Dm6JmjKomIU-QmPRIiTy-e1hFrKbhQM6Y_mN8zMQaj3iWTaWrOdt1SBor');
        $msg = array(
            'body' => $message,
            'title' => $title,
            'image' => 'https://vivilanoche.com/uploads/images/promocion/' . $promocion->url_imagen,
            'sound' => 'default',
            'vibrate' => '1000',
        );
        $data = array(
            'id_promocion' => $promocion->id_promocion,
            'id_negocio' => $promocion->id_negocio,
            'url_imagen' => $promocion->url_imagen,
            'nombre' => $promocion->nombre,
            'descripcion' => $promocion->descripcion,
        );
        $fields = array(
            //'to' => "c30JiyfgAxs:APA91bH2BiQNz-SSYYngJqMxtp_8qoarn22XW_Fr4SziwkPqno0b_2Z5Qqy8oYkxFKGK9gpxsN2I46ce4CnnPPrYudOG8HrTprKeUbB0I1H_PB_TnhMBUNNI1Zwxj2I-ReWdfGl0toMM",
            'to' => "/topics/news",
            'notification' => $msg,
            'data' => $data,
        );
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
    }

}
