<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\utils;

/**
 * Description of UtilHtml
 *
 * @author RAUL CORIMAYO
 */
class UtilHtml {
    
    public static function paragraph($txt){
        $explode = explode("\n", $txt);
        $html = '';
        foreach ($explode as $line) {
            $html .= '<p style="text-align:justify;">'.$line.'</p>';
        }
        return $html;
    }
    
}
