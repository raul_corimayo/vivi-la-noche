<?php

namespace common\utils;

class UtilFecha {

    public static function toYMD($fecha) {
        if($fecha){
            $values = self::getValues($fecha);
            return $values['Y'].'-'.$values['m'].'-'.$values['d'];
        }
        return null;
    }

    public static function toDMYHMS($ymdhms) {
        $explode = explode(' ', $ymdhms);
        $fecha = $explode[0];
        $fechaDMY = self::toDMY($fecha);
        $hora = $explode[1];
        return "$fechaDMY $hora";
    }

    public static function toDMY($fecha) {
        if($fecha){
            $values = self::getValues($fecha);
            return $values['d'].'/'.$values['m'].'/'.$values['Y'];
        }
        return null;
    }

    private static function getValues($fecha){
        $valuesDMY = self::getValuesDMY($fecha, '/');
        if($valuesDMY){
            return $valuesDMY;
        }
        $valuesYMD = self::getValuesYMD($fecha, '-');
        if($valuesYMD){
            return $valuesYMD;
        }
        throw new Exception('Formato de fecha no valido');
        //return $fecha;
    }
    
    private static function getValuesDMY($fecha, $separador) {
        $explode = explode($separador, $fecha);
        if(count($explode) == 3){
            return [
                'd'=>$explode[0],
                'm'=>$explode[1],
                'Y'=>$explode[2],
            ];
        }
        return [];
    }
    
    private static function getValuesYMD($fecha, $separador) {
        $explode = explode($separador, $fecha);
        if(count($explode) == 3){
            return [
                'd'=>$explode[2],
                'm'=>$explode[1],
                'Y'=>$explode[0],
            ];
        }
        return [];
    }

}
