<?php

namespace common\utils;
use Yii;

/**
 * Description of Util
 *
 * @author RAUL
 */
class UtilImage {
    
    public static function getDeletedModelMessage(){
        return Yii::$app->params['deleteModelMessage'];
    }
    
    
    public static function getFolderCategoriaIcon(){
        return self::normalizeUrlImage(Yii::$app->params['url_categoria_icon']);
    }
    
    public static function getFolderNegocioImagen(){
        return self::normalizeUrlImage(Yii::$app->params['url_negocio_imagen']);
    }
    
    public static function getFolderNoticiaImagen(){
        return self::normalizeUrlImage(Yii::$app->params['url_noticia_imagen']);
    }
    
    public static function getFolderPromocionImagen(){
        return self::normalizeUrlImage(Yii::$app->params['url_promocion_imagen']);
    }
    
    private static function normalizeUrlImage($url){
        $base = Yii::$app->basePath;
        if(strpos($base, 'frontend')){
            return ''.$url;
        }
        else if(strpos($base, 'backend')){
            return '../'.$url;
        }
        echo 'ERROR: Util. URL image. ';
        echo $base;
        die;
    }
    
    public static function uploadImage($model, $folder, $attrName){
        $image = \yii\web\UploadedFile::getInstance($model, $attrName);
        if (!is_null($image)) {
            $explode = explode(".", $image->name);
            $ext = end($explode);
            $image_web_filename = Yii::$app->security->generateRandomString() . ".{$ext}";
            //$folderCategoriaIcon = \common\utils\Util::getFolderCategoriaIcon();
            Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/' . $folder;
            $path = Yii::$app->params['uploadPath'] . $image_web_filename;
            $model->$attrName = $image_web_filename;
            $image->saveAs($path);
            return true;
        }
        return false;
    }
    
    public static function deleteImage($folder, $image){
        //unlink(Yii::$app->basePath.'/'.\common\utils\UtilImage::getFolderCategoriaIcon().'wU1nlFHEgUOUNCZ4hIYjhCXrognFkdHW.png');
        unlink(Yii::$app->basePath.'/'.$folder.$image);
    }
    
}
