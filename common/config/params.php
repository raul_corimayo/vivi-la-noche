<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    //
    'deleteModelMessage' => 'Seguro que desea eliminar este registro?',
    'url_categoria_icon' => 'uploads/images/categoria/',
    'url_negocio_imagen' => 'uploads/images/negocio/',
    'url_noticia_imagen' => 'uploads/images/noticia/',
    'url_promocion_imagen' => 'uploads/images/promocion/',
    //
];
