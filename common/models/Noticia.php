<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "noticias".
 *
 * @property int $id_noticia
 * @property string $creacion
 * @property string $publicacion
 * @property string $titulo
 * @property string $url_imagen
 * @property string $contenido
 */
class Noticia extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['creacion', 'publicacion'], 'safe'],
            [['publicacion', 'titulo'/*, 'url_imagen'*/, 'contenido'], 'required'],
            //[['url_imagen'], 'required', 'on'=>'create'],
            
            [['contenido'], 'string'],
            [['titulo'], 'string', 'max' => 50],
            [['url_imagen'], 'string', 'max' => 255],
            
            // image
            [['url_imagen'], 'required', 'on'=>'create'],
            [['url_imagen'], 'safe'],
            [['url_imagen'], 'file', 'extensions'=>'jpg, gif, png'],
            //[['url_imagen'], 'file', 'maxSize'=>'100000'],// set max size 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_noticia' => 'Id Noticia',
            'creacion' => 'Creación',
            'publicacion' => 'Publicación',
            'titulo' => 'Título',
            'url_imagen' => 'Url Imagen',
            'contenido' => 'Contenido',
        ];
    }
    
    public function __toString() {
        return $this->titulo;
    }
    
    public function getUrlImagen(){
        return \common\utils\UtilImage::getFolderNoticiaImagen().$this->url_imagen;
    }
    
    public static function getAll($countNoticias = -1){
        $hoy = date('Y-m-d');
        return self::find()
                ->where("publicacion <= '$hoy'")
                ->orderBy('publicacion desc')
                ->limit($countNoticias)
                ->all()
                ;
    }
    
    public function getContenidoHtml(){
        return \common\utils\UtilHtml::paragraph($this->contenido);
    }

    
}
