<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "categorias".
 *
 * @property int $id_categoria
 * @property string $nombre
 * @property int $orden
 * @property string $url_icono
 */
class Categoria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'orden'/*, 'url_icono'*/], 'required'],
            [['orden'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['url_icono'], 'string', 'max' => 255],
            // image
            [['url_icono'], 'required', 'on'=>'create'],
            [['url_icono'], 'safe'],
            [['url_icono'], 'file', 'extensions'=>'jpg, gif, png'],
            //[['url_icono'], 'file', 'maxSize'=>'100000'],// set max size 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_categoria' => 'Categoría',
            'nombre' => 'Nombre',
            'orden' => 'Orden',
            'url_icono' => 'Ícono',
        ];
    }
    
    public function __toString() {
        return $this->nombre;
    }
    
    public function getUrlIcono(){
        return \common\utils\UtilImage::getFolderCategoriaIcon().$this->url_icono;
    }
}
