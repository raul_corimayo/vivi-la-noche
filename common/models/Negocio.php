<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "negocios".
 *
 * @property int $id_negocio
 * @property int $id_ciudad
 * @property int $id_categoria
 * @property string $nombre
 * @property string $url_imagen
 * @property string $url_video
 * @property string $geo_latitud
 * @property string $geo_longitud
 */
class Negocio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'negocios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_ciudad', 'id_categoria', 'nombre', /*'url_imagen', */'url_video', 'geo_latitud', 'geo_longitud'], 'required'],
            [['id_ciudad', 'id_categoria'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['url_imagen', 'url_video'], 'string', 'max' => 255],
            [['geo_latitud', 'geo_longitud'], 'string', 'max' => 50],
            // image
            [['url_imagen'], 'required', 'on'=>'create'],
            [['url_imagen'], 'safe'],
            [['url_imagen'], 'file', 'extensions'=>'jpg, gif, png'],
            //[['url_imagen'], 'file', 'maxSize'=>'100000'],// set max size 
            // validator
            ['url_video', 'idVideoValidate'],
        ];
    }

    public function idVideoValidate($attribute, $params, $validator){
        if( ! $this->idVideoValidator()){
            $this->addError('url_video', 'Url invalida. La url debe tener la forma https://www.youtube.com&v=ID_DE_11_CARACTERES');
        }
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_negocio' => 'Negocio',
            'id_ciudad' => 'Ciudad',
            'id_categoria' => 'Categoria',
            'nombre' => 'Nombre',
            'url_imagen' => 'Imagen',
            'url_video' => 'ID del Video de youtube',
            'geo_latitud' => 'Geo Latitud',
            'geo_longitud' => 'Geo Longitud',
        ];
    }
    
    public function __toString() {
        return $this->nombre;
    }
    
    public function getCiudad(){
        return $this->hasOne(Ciudad::className(), ['id_ciudad' => 'id_ciudad']);
    }
    
    public function getCategoria(){
        return $this->hasOne(Categoria::className(), ['id_categoria' => 'id_categoria']);
    }
    
    public function getUrlImagen(){
        return \common\utils\UtilImage::getFolderNegocioImagen().$this->url_imagen;
    }
    
    /////////////////////////////////////
    
    public function idVideoValidator(){
        $exp = explode("?v=", $this->url_video);
        if(count($exp) != 2){
            return false;
        }
        $videoID = $exp[1];
        if(strlen($videoID) != 11){
            return false;
        }
        return true;
    }
    
    public function getVideoID(){
        $exp = explode("?v=", $this->url_video);
        if(count($exp) >= 2){
            return $exp[1];
        }
        return '';
    }
}
