<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "promociones".
 *
 * @property int $id_promocion
 * @property string $nombre
 * @property int $id_negocio
 * @property string $url_imagen
 * @property string $descripcion
 */
class Promocion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promociones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'id_negocio', /*'url_imagen', */], 'required'],
            [['id_negocio'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['url_imagen'], 'string', 'max' => 255],
            [['descripcion'], 'string'],
            // image
            [['url_imagen'], 'required', 'on'=>'create'],
            [['url_imagen'], 'safe'],
            [['url_imagen'], 'file', 'extensions'=>'jpg, gif, png'],
            //[['url_imagen'], 'file', 'maxSize'=>'100000'],// set max size 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_promocion' => 'Id Promocion',
            'nombre' => 'Nombre',
            'id_negocio' => 'Negocio',
            'url_imagen' => 'Imagen',
            'descripcion' => 'Descripción',
        ];
    }
    
    public function __toString() {
        return $this->nombre;
    }
    
    public function getNegocio(){
        return $this->hasOne(Negocio::className(), ['id_negocio' => 'id_negocio']);
    }
    
    public function getUrlImagen(){
        return \common\utils\UtilImage::getFolderPromocionImagen().$this->url_imagen;
    }
    
    public function publicar(){
        \common\utils\UtilFirebase::publicarPromocion($this);
    }
    
}
