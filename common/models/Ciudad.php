<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ciudades".
 *
 * @property int $id_ciudad
 * @property string $nombre
 */
class Ciudad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ciudades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ciudad' => 'Id Ciudad',
            'nombre' => 'Nombre',
        ];
    }
    
    public function __toString() {
        return $this->nombre;
    }
    
}
