<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Negocio;

/**
 * NegocioSearch represents the model behind the search form of `common\models\Negocio`.
 */
class NegocioSearch extends Negocio
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_negocio', 'id_ciudad', 'id_categoria'], 'integer'],
            [['nombre', 'url_imagen', 'url_video', 'geo_latitud', 'geo_longitud'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Negocio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_negocio' => $this->id_negocio,
            'id_ciudad' => $this->id_ciudad,
            'id_categoria' => $this->id_categoria,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'url_imagen', $this->url_imagen])
            ->andFilterWhere(['like', 'url_video', $this->url_video])
            ->andFilterWhere(['like', 'geo_latitud', $this->geo_latitud])
            ->andFilterWhere(['like', 'geo_longitud', $this->geo_longitud]);

        return $dataProvider;
    }
}
